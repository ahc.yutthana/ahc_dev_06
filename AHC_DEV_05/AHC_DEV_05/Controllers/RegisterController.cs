﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto;
using AHCBL.Dto.Admin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers
{
    public class RegisterController : Controller
    {
        // GET: Register
        //public ActionResult Index()
        //{
        //    ViewBag.bank_id = DropdownDao.Instance.GetDataBank();
        //    return View();
        //}
        public ActionResult Index(string code)
        {
            if (code != null)
            {
                //ViewBag.adviser = DataCryptography.Decrypt(Token);
                ViewBag.adviser = code;
                string member_id ="";
                LoginDao.Instance.ChkUser(code, ref member_id);
                ViewBag.member_id = member_id;
            }
            ViewBag.bank_id = DropdownDao.Instance.GetDataBank();
            return View();
        }
        [HttpPost]
        public ActionResult Index(MemberListDto model)
        {
            try
            {
                int member_id = 0;
                string pUName = string.Empty;
                int member_level = 0;
                string password = string.Empty;
                string amt = string.Empty;
                                    
                string result = MemberListDao.Instance.SaveDataList(model, "add", ref member_id);
                if (result != "OK")
                {
                    ViewBag.Message = result;
                    ModelState.Clear();
                }
                else
                {
                    //가입포인트
                    ViewBag.bank_id = DropdownDao.Instance.GetDataBank();
                    //ModelState.Clear();
                    if (LoginDao.Instance.ChkAuthDB(model.username, ref member_id, ref pUName, ref member_level, ref password, ref amt))
                    {
                        EUser user = new EUser();
                        user.UserIP = GetUserIP();
                        user.member_id = member_id;
                        user.UserID = model.username;
                        user.UserName = pUName;
                        user.Login = DateTime.Now;

                        Varible.User = user;
                        Varible.User.member_id = member_id;
                        Varible.User.lelvel = member_level;
                        Varible.User.amount = amt;
                        Varible.User.pass = model.password;

                        Session["username"] = Varible.User.UserName;
                        var da = Request.Browser.IsMobileDevice;
                        LogDto log = new LogDto();
                        log.ip = user.UserIP;
                        log.url = Request.Url.AbsoluteUri;
                        log.browser = GetWebBrowserName();
                        log.system_os = Info.GetHardware();
                        log.connect_hardware = Request.Browser.IsMobileDevice == true ? "Mobile" : "Computer";
                        //log.member_id = Util.NVLInt(Varible.User.member_id);
                        LoginDao.Instance.LogLogin(log, "login");
                        if (member_level == 2)
                        {
                            Response.Redirect("~/Main", false);
                        }
                    //Response.Redirect("~/Main", false);
                    }
                }
                return View();
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error : " + e.Message;
                return View();
            }
        }
        public ActionResult ChkUserID(string pUid)
        {
            try
            {

                string member_id = string.Empty;
                //Session.Remove("member_id");
                bool result = LoginDao.Instance.ChkUser(pUid, ref member_id);
                if (result == false)
                {
                    return Json(new returnsave { err = "1", errmsg = "사용 가능한 아이디 입니다.\n회원가입을 진행해 주세요." });
                }
                else
                {
                    //Session["member_id"] = member_id;
                    return Json(new returnsave { err = member_id, errmsg = "사용중인 아이디 입니다. 다른 아이디를 입력해 주세요." });
                }
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "0", errmsg = "Error" });
            }


            //return objreturnsave;
        }
        [HttpPost]
        public ActionResult ChkAdviser(string pUid)
        {
            try
            {

                string member_id =string.Empty;
                //Session.Remove("member_id");
                bool result = LoginDao.Instance.ChkUser(pUid, ref member_id);
                if (result == false)
                {
                    return Json(new returnsave { err = "0", errmsg = "추천인을 찾을수 없습니다. 추천인 아이디를 확인해 주세요." });
                }
                else
                {
                    //Session["member_id"] = member_id;
                    return Json(new returnsave { err = member_id, errmsg = "추천인이 확인되었습니다.\n회원가입을 진행해 주세요." });
                }
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "0", errmsg = "Error" });
            }


            //return objreturnsave;
        }
        private string GetUserIP()
        {
            string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList))
            {
                return ipList.Split(',')[0];
            }

            return Request.ServerVariables["REMOTE_ADDR"];
        }
        public string GetWebBrowserName()
        {
            string WebBrowserName = string.Empty;
            try
            {
                WebBrowserName = Request.Browser.Browser;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return WebBrowserName;
        }
    }
}