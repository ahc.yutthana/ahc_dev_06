﻿using AHCBL.Component.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class BoardController : Controller
    {
        Permission checkuser = new Permission();
        // GET: Board
        public ActionResult Index()
        {
            checkuser.chkrights("admin");
            ViewBag.Count = 0;
            ViewBag.rows = 0;
            return View();
        }
    }
}