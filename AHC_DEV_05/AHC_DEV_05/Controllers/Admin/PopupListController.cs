﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class PopupListController : Controller
    {
        Permission checkuser = new Permission();
        // GET: PopupList
        public ActionResult Index(int? page)
        {
            checkuser.chkrights("admin");
            var data = PopupListDao.Instance.GetDataList();
            ViewBag.Count = data.Count.ToString();
            return View(data.ToList().ToPagedList(page ?? 1, Util.NVLInt(ConfigurationManager.AppSettings["rows"])));
        }

        public ActionResult Create()
        {
            //checkuser.chkrights("BoardList");
            ViewBag.group_id = DropdownDao.Instance.GetDataGroup();
            PopupListDto model = new PopupListDto();
            model.disable_hours = ConfigurationManager.AppSettings["pop_time"];
            model.pop_top = ConfigurationManager.AppSettings["pop_top"];
            model.pop_width = ConfigurationManager.AppSettings["pop_width"];
            model.pop_height = ConfigurationManager.AppSettings["pop_height"];
            model.pop_left = ConfigurationManager.AppSettings["pop_left"];
            ViewBag.drv_id = DropdownDao.Instance.GetDataDrive();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(PopupListDto model)
        {

            try
            {
                ViewBag.group_id = DropdownDao.Instance.GetDataGroup();
                ViewBag.drv_id = DropdownDao.Instance.GetDataDrive();
                if (ModelState.IsValid)
                {

                    string result = PopupListDao.Instance.SaveDataList(model, "add");
                    if (result != "OK")
                    {
                        ViewBag.Message = result;
                        ModelState.Clear();
                    }
                    else
                    {
                        
                        ViewBag.Message = "Successfully !!";
                        ModelState.Clear();
                        model = new PopupListDto();
                        model.disable_hours = ConfigurationManager.AppSettings["pop_time"];
                        model.pop_top = ConfigurationManager.AppSettings["pop_top"];
                        model.pop_width = ConfigurationManager.AppSettings["pop_width"];
                        model.pop_height = ConfigurationManager.AppSettings["pop_height"];
                        model.pop_left = ConfigurationManager.AppSettings["pop_left"];
                    }
                }
                else
                {
                    ViewBag.Message = "Data not found !";
                }
                return View(model);
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error : " + e.Message;
                return View();
            }
        }
        public ActionResult Edit(int id)
        {
            ViewBag.group_id = DropdownDao.Instance.GetDataGroup();
            ViewBag.drv_id = DropdownDao.Instance.GetDataDrive();
            var data = PopupListDao.Instance.GetDataList().Find(smodel => smodel.id == id);
            data.start_date = data.start_date.Substring(0,10);
            data.stop_date = data.stop_date.Substring(0, 10);
            return View(data);
        }

        [HttpPost]
        public ActionResult Edit(PopupListDto model)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    string result = PopupListDao.Instance.SaveDataList(model, "edit");
                    if (result != "OK")
                    {
                        ViewBag.Message = result;
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    ViewBag.group_id = DropdownDao.Instance.GetDataGroup();
                    ViewBag.drv_id = DropdownDao.Instance.GetDataDrive();
                   
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(PopupListDto model)
        {
            try
            {
                //model.id = id;
                PopupListDao.Instance.SaveDataList(model, "del");
                return Json("Delete Successfully !");
            }
            catch (Exception e)
            {
                return Json("Error");
            }
            //}
            //try
            //{
            //    string result = PopupListDao.Instance.SaveDataList(model, "del");
            //    if (result == "OK")
            //    {
            //        ViewBag.Message = "Student Deleted Successfully";
            //    }
            //    return RedirectToAction("Index");
            //}
            //catch (Exception e)
            //{
            //    ViewBag.Message = "Error : " + e.Message.ToString();
            //    return View();
            //}
        }
    }
}
