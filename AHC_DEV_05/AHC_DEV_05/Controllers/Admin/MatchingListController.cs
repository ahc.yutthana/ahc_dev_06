﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class MatchingListController : Controller
    {
        // GET: MatchingList
        public ActionResult Index(int? page, string keyword)
        {
            var data = MatchingListDao.Instance.GetData();
            int rows = Util.NVLInt(Varible.Config.page_rows);
            TempData["data"]= data.Where(x => x.username_sale == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
            Session["Status"] = DropdownDao.Instance.GetMathcingStatus();
            string count = string.Empty;
            if (data.Count < 1000)
            {
                count = data.Count.ToString();
            }
            else
            {
                count = data.Count.ToString("0,0", CultureInfo.InvariantCulture);
            }
            ViewBag.Status = Session["Status"];
            ViewBag.Count = count;
            ViewBag.Rows = rows;
            return View(TempData["data"]);


        }
        public ActionResult Updatedata(string status,int id)
        {
            try
            {

                ProductOrderDto model = new ProductOrderDto();
                model.id = id;
                if (Convert.ToInt32(status) == 0)
                {
                    string result = MatchingListDao.Instance.SaveMatchingStatus(model);
                    if (result != "OK")
                    {
                        return Json(result);
                    }
                    else
                    {
                        return Json("Successfully !");
                    }
                }
                else
                {
                    return Json("Not Update Data status is not 0!");
                }
            }
            catch (Exception e)
            {
                return Json("Error !!");
            }
        }
    }
}