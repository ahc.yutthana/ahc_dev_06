﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class RequestBuyController : Controller
    {
        // GET: RequestBuy
        public ActionResult Index(string drp, string keyword, int? page)
        {
            Session["drp"] = DropdownDao.Instance.GetDrpReqBuy();
            ViewBag.Search = Session["drp"];

            //ViewBag.Status = DropdownDao.Instance.GetDrpReqBuy();
            var data = RequestBuyDao.Instance.GetDataList();
            int rows = Util.NVLInt(Varible.Config.page_rows);
            TempData["data"] = data.ToList().ToPagedList(page ?? 1, rows);
            TempData["data1"] = data.ToList();
            string count = string.Empty;
           
            if (drp == "username")
            {
                TempData["data"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                TempData["data1"] = data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList();
                //return View(TempData["data"]);
            }
            if (drp == "amount")
            {
                TempData["data"] = data.Where(x => x.amount == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                TempData["data1"] = data.Where(x => x.amount == keyword || keyword == null || keyword == "").ToList();
                //return View(TempData["data"]);
            }
            if (drp == "product_name")
            {
                TempData["data"] = data.Where(x => x.product_name == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                TempData["data1"] = data.Where(x => x.product_name == keyword || keyword == null || keyword == "").ToList();
                //return View(TempData["data"]);
            }            
            if (drp == "buy_date")
            {
                TempData["data"] = data.Where(x => x.process == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                TempData["data1"] = data.Where(x => x.process == keyword || keyword == null || keyword == "").ToList();
                //return View(TempData["data"]);
            }
            if (data.Count < 1000)
            {
                count = data.Count.ToString();
            }
            else
            {
                count = data.Count.ToString("0,0", CultureInfo.InvariantCulture);
            }

            ViewBag.Count = count;
            ViewBag.Rows = rows;

            Session["data"] = TempData["data1"];
            return View(TempData["data"]);
            //return PartialView(data);
            //return View(data.ToList().ToPagedList(page ?? 1, rows));

        }
       

        [HttpPost]
        public ActionResult getdata(int orderId, int productId, int member)
        {
            try
            {

                string html = "";
                var Matching = RequestBuyDao.Instance.GetDataMatching(orderId,productId, member);
                if (Matching.Count > 0) {
                    for (int i = 0; i < Matching.Count; i++)
                    {
                        html += "<tr class='text-center'>" +
                            "<td> " + Matching[i].num + "</td>" +
                            "<td> " + Matching[i].username + "</td>" +
                            "<td> " + Matching[i].fullname + "</td>" +
                            "<td> " + Matching[i].product_name + " </td>" +
                            "<td> " + Matching[i].amount + "</td>" +
                            "<td> " + Matching[i].price + " </td>" +
                            "<td> " + Matching[i].buy_date + " </td>" +
                            "<td> <input type='button' value='Match' class='btn btn-secondary' onclick='matchdata(" + Matching[i].id + "," + Matching[i].amount + ")' /></td>" +
                        "</tr>";
                    }
                }
                else
                {
                    html += "<tr class='text-center'>" +
                                "<td colspan='8'>" +
                                    "No Data !!" +
                                "</td> " +
                            "</tr> ";
                }

                return Json(new returnsave { err = "0", errmsg = html });

            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "1", errmsg = "Error" });
            }

        }
        public ActionResult matchdata(int id, int id_sale, int amt_sale)
        {
            try
            {
                //Session.Remove("member_id");dddd
                string result = RequestBuyDao.Instance.ChkData(id, id_sale, amt_sale);
                if (result == "0")
                {
                    return Json(new returnsave { err = "1", errmsg = "Error. !" });
                }
                else if (result == "1")
                {

                    return Json(new returnsave { err = "0", errmsg = "Successful. !" });
                }
                else if (result == "2")
                {
                    return Json(new returnsave { err = "0", errmsg = "Successful. !" });
                }
                else
                {
                    return Json(new returnsave { err = "0", errmsg = "Successful. !" });
                }
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "0", errmsg = "Error. !" });
            }
        }

    }
}