﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto.Admin
{
    public class VisitListDto
    {
        public int id { get; set; }
        public string ip { get; set; }
        public string name { get; set; }
        public string browser { get; set; }
        public string os { get; set; }
        public string drive { get; set; }
        public string create_date { get; set; }
        public string start_date { get; set; }
        public string stop_date { get; set; }
    }
}
