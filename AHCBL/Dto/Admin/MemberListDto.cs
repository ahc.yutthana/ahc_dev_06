﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AHCBL.Dto.Admin
{
    public class MemberListDto
    {
        [Display(Name = "id")]
        public int id { get; set; }

        [Required(ErrorMessage = "Please enter.")]
        [MaxLength(20)]
        public string username { get; set; }

        [MaxLength(50)]
        public string password { get; set; }

        [MaxLength(20)]
        public string fullname { get; set; }

        [MaxLength(20)]
        public string nickname { get; set; }

        public int level { get; set; }
        public int member_no { get; set; }

        [MaxLength(11)]
        public string point { get; set; }

        [MaxLength(100)]
        public string email { get; set; }

        [MaxLength(255)]
        public string homepage { get; set; }

        [MaxLength(20)]
        public string telephone { get; set; }

        [MaxLength(20)]
        public string mobile { get; set; }
        public int certify_case { get; set; }
        public int certify { get; set; }
        public int adult { get; set; }

        [MaxLength(6)]
        public string address { get; set; }

        [MaxLength(255)]
        public string address1 { get; set; }

        [MaxLength(255)]
        public string address2 { get; set; }

        [MaxLength(255)]
        public string address3 { get; set; }

        [MaxLength(255)]
        public string icon { get; set; }

        [MaxLength(255)]
        public string img { get; set; }
        public string mailling { get; set; }
        public string sms { get; set; }
        public string open { get; set; }

        [MaxLength(255)]
        public string signature { get; set; }

        [MaxLength(255)]
        public string profile { get; set; }

        [MaxLength(255)]
        public string memo { get; set; }
        public string create_date { get; set; }
        //public string log_date { get; set; }
        //public string log_ip { get; set; }

        [MaxLength(20)]
        public string adviser_name { get; set; }
        public int adviser { get; set; }
        public string leave_date { get; set; }
        public string intercept_date { get; set; }

        [MaxLength(10)]
        public string ip { get; set; }
        public int bank_id { get; set; }

        [MaxLength(20)]
        public string acc_no { get; set; }

        [MaxLength(255)]
        public string acc_name { get; set; }
        public int member_amount { get; set; }
        public string last_date { get; set; }

        [MaxLength(255)]
        public string txt1 { get; set; }

        [MaxLength(255)]
        public string txt2 { get; set; }

        [MaxLength(255)]
        public string txt3 { get; set; }

        [MaxLength(255)]
        public string txt4 { get; set; }

        [MaxLength(255)]
        public string txt5 { get; set; }

        [MaxLength(255)]
        public string txt6 { get; set; }

        [MaxLength(255)]
        public string txt7 { get; set; }

        [MaxLength(255)]
        public string txt8 { get; set; }

        [MaxLength(255)]
        public string txt9 { get; set; }

        [MaxLength(255)]
        public string txt10 { get; set; }
        public string active { get; set; }
        public string status { get; set; }
        public SelectList Techniques { get; set; }
        public SelectList bank_list { get; set; }
    }
}

