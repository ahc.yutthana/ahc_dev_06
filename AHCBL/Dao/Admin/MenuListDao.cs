﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.Admin
{
    public class MenuListDao : BaseDao<MenuListDao>
    {
        private MySqlConnection conn;
        //private DataTable dt;
        public List<MenuListDto> GetDataList()
        {
            try
            {
                List<MenuListDto> list = new List<MenuListDto>();
                //DataTable dt = GetStoredProc("PD019_GET_MENU", new string[] { "@member_id" }, new string[] { Util.NVLString(Varible.User.member_id) });
                DataTable dt = GetStoredProc("PD019_GET_MENU", new string[] { "@member_id" }, new string[] { Util.NVLString(1) });
                int j = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    DataTable dt1 = GetStoredProc("PD020_GET_MENU_LIST", new string[] { "@member_id", "@p_id" }, new string[] { Util.NVLString(1), Util.NVLString(dr["id"]) });
                    list.Add(
                    new MenuListDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        name = Util.NVLString(dr["name"]),
                        icon = dr["icon"].ToString()
                    });
                    foreach (DataRow dr1 in dt1.Rows)
                    {
                        list[j].sub_menu.Add(new MenuList
                        {

                            name = Util.NVLString(dr1["name"]),
                            url = Util.NVLString(dr1["url"])
                        });
                    }

                    j++;
                }


                return list;
            }
            catch(Exception ex)
            {
                throw ex;
            }

         }
    }
}
