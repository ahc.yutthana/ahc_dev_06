﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.Admin
{
    public class MatchingListDao : BaseDao<MatchingListDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<ProductOrderDto> GetDataList()
        {
            try
            {
                List<ProductOrderDto> list = new List<ProductOrderDto>();
                //dt = GetStoredProc("PD009_GET_CATEGORY");
                dt = GetStoredProc("PD087_GET_MATCHING_LIST");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new ProductOrderDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        num = Util.NVLInt(dr["num"]),
                        code = Util.NVLString(dr["code"]),
                        username_buy = Util.NVLString(dr["username_buy"]),
                        fullname_buy = Util.NVLString(dr["fullname_buy"]),
                        username_sale = Util.NVLString(dr["username_sale"]),
                        fullname_sale = Util.NVLString(dr["fullname_sale"]),
                        product_name = Util.NVLString(dr["product_name"]),
                        amount = Util.NVLInt(dr["amount"]),
                        price_interest = Util.NVLString(Util.NVLDecimal(dr["price_interest"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        fee = Util.NVLString(Util.NVLDecimal(dr["fee"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        buy_date = dr["buy_date"].ToString()=="" ? "" : Util.NVLString(Cv.Date(Convert.ToDateTime(dr["buy_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        status = Util.NVLString(dr["status"]),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public List<ProductOrderDto> GetData()
        {
            try
            {
                List<ProductOrderDto> list = new List<ProductOrderDto>();
                //dt = GetStoredProc("PD009_GET_CATEGORY");
                dt = GetStoredProc("PD089_MATCHING_LIST");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new ProductOrderDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        num = Util.NVLInt(dr["num"]),
                        code = Util.NVLString(dr["code"]),
                        username_buy = Util.NVLString(dr["username_buy"]),
                        fullname_buy = Util.NVLString(dr["fullname_buy"]),
                        username_sale = Util.NVLString(dr["username_sale"]),
                        fullname_sale = Util.NVLString(dr["fullname_sale"]),
                        product_name = Util.NVLString(dr["product_name"]),
                        amount = Util.NVLInt(dr["amount"]),
                        status = Util.NVLString(dr["status"]),
                        price_interest = Util.NVLString(Util.NVLDecimal(dr["price_interest"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        fee = Util.NVLString(Util.NVLDecimal(dr["fee"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        //buy_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["buy_date"]).ToString("yyyyMMddHH:mm:ss"))),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public string SaveMatchingStatus(ProductOrderDto model)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD090_SAVE_MATCHING_STATUS", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }

    }
}